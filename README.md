# STARTING
## 1 - Install Docker and Docker-compose on the Server(If Necessary)

Installing Docker the correct way without using any package manager is necessary otherwise you will run into permission issue

Follow the link https://docs.docker.com/install/linux/docker-ce/ubuntu/ and 

``` bash
sudo apt-get update
sudo apt-get install \
    apt-transport-https \
    ca-certificates \
    curl \
    gnupg-agent \
    software-properties-common
curl -fsSL https://download.docker.com/linux/ubuntu/gpg | sudo apt-key add -
sudo add-apt-repository \
   "deb [arch=amd64] https://download.docker.com/linux/ubuntu \
   $(lsb_release -cs) \
   stable"
sudo apt-get update
sudo apt-get install docker-ce docker-ce-cli containerd.io
```

## 2 - Install Redis on the Server(If Necessary)

You will need Redis for the Cache and Session engine, follow this link to install redis, you can stop on STEP 2
https://tecadmin.net/install-redis-ubuntu/
``` bash
sudo apt-get update 
sudo apt-get upgrade
sudo apt-get install redis-server
sudo systemctl enable redis-server.service
```

## 3 - Install Percona SQL on the Server(If Necessary)

Install de percona SQL, using this link: https://www.percona.com/doc/percona-server/5.7/installation/apt_repo.html#installing-percona-server-from-percona-apt-repository
It will ask for an username and password, you might be using this username and password latter on; as default we normaly do user `root` and the password the same provided by the client on the **server password**;
After the creation of the SQL server you can also Create the database that will be used for WDA to work.

``` bash
sudo apt-get install gnupg2
curl -O https://repo.percona.com/apt/percona-release_latest.generic_all.deb
sudo apt install -y gnupg2 lsb-release ./percona-release_latest.generic_all.deb
sudo apt-get update
sudo percona-release setup ps80
sudo percona-release enable ps-80 release
sudo apt update
sudo apt install -y percona-server-server
```
After installing, the mysql run the command to make it more secure, and remove the pending files.
``` bash
sudo mysql_secure_installation
rm percona-release_latest.bionic_all.deb 
```

**Create database - Use this config to use the correct collate and charset**
```
mysql -uroot -p
CREATE DATABASE wda_<CLIENTE> CHARACTER SET utf8mb4 DEFAULT COLLATE utf8mb4_unicode_ci;
CREATE USER 'wda_<CLIENTE>'@'localhost' IDENTIFIED BY 'password';
GRANT ALL PRIVILEGES ON wda_<CLIENTE>.* TO 'wda_<CLIENTE>'@'localhost';
FLUSH PRIVILEGES;

exit;
```

## 4 - Configure WDA to work

On your root server create a folder, if necessary make sure this folder is mapped to the HDD that you will be runing backups
```
mkdir www_root
cd www_root
```

Clone the Webdjangulardeploy repo
```
git clone https://r4t40@bitbucket.org/myowngames/wdadeploy.git wda
cd wda
```
Change copy and paste the .env.demo to .env file and change any configuration necessary on the .env file
```
cp .env.demo .env
```
Lets run the first time to make sure it's all working
```
docker-compose up
```
This will now start the docker but you will be able to see everything that's going on

# Making the service of the Docker compose (wda-app)
Making the service of the WDA will make sure it will start on its own if for some reasons somethings breaks.
The file wda-app.service is the service configuration file, if for some rease the docker-compose.yaml is not in the /www_root/wda/docker-compose.yaml
please change the path of the wda-app.service *WorkingDirectory* 
After changing the file necessarly copy it to the path:
```
cp ./wda-app.service /etc/systemd/system/wda-app.service 
```
Now activate the service
```
systemctl enable wda-app
```
And you now can run the service
```
service wda-app start
```
To make sure everything is working, reboot the server and make sure it will start all wda-app services byt it self
```
service wda-app status
```

# Updating Docker containers
To pull new containers updates to the docker before we restart de service
This command will pull all the required images
```
docker compose pull
```
This command will restart the service using the new version of the container
```
service wda-app restart
```

# Checking Services with docker
running the following command will show all ACTIVE docker containers
```
docker ps
```
runging the following command wil show ALL docker containers
```
docker ps -a
```



## 5 - Install Nginx on the Server(If Necessary)

 - Install Nginx https://www.nginx.com/resources/wiki/start/topics/tutorials/install/:
```
sudo apt-get update
sudo apt-get install nginx
```
 - Configure Nginx:
Copy from this folder `cp ./config/nginx/mime.types /etc/nginx/mime.types`
Copy from this folder `cp ./config/nginx/domain.com.conf /etc/nginx/sites-enabled/<domain>.conf` (replace <domain> to the user domain)

Also Activate GZIP, uncomenting the GZIP lines on the nginx.conf

Now edit the configuration file for your current domain `<domain>.conf` 
Replace `${ALLOWED_HOSTS}`, and make sure you need the SSL otherwise you can comment line 8 til 14 and add `listen 80;` between line 16 an 17
Replace `${NGINX_SSL_KEY}`, for the SSL key path;  `${NGINX_SSL_CERT}`, for the SSL Cert Path; if necessary

The file on the path config

## 6 - Configurar Nginx com Let's Encrypt
Fonte: https://www.digitalocean.com/community/tutorials/how-to-secure-nginx-with-let-s-encrypt-on-ubuntu-20-04

Instalando Certbot
```
sudo apt install certbot python3-certbot-nginx
```
Obter o certificado, trocar o examle.com pelo o seu link.
Esse link tem que estar configurado no arquivo de configuração de nginx feito anteriormente.
```
sudo certbot --nginx -d example.com -d www.example.com
```

Configurar o outo Renewal
````
sudo systemctl status certbot.timer
````
Output:
```
Output
● certbot.timer - Run certbot twice daily
     Loaded: loaded (/lib/systemd/system/certbot.timer; enabled; vendor preset: enabled)
     Active: active (waiting) since Mon 2020-05-04 20:04:36 UTC; 2 weeks 1 days ago
    Trigger: Thu 2020-05-21 05:22:32 UTC; 9h left
   Triggers: ● certbot.service
```
# TODO: Create Backup Routine